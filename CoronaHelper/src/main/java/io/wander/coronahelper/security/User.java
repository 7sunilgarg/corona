package io.wander.coronahelper.security;

import java.util.concurrent.atomic.AtomicInteger;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class User {

	
	static AtomicInteger ids = new AtomicInteger(100000); // starting from big enough so that auto generation does not collide with it
	
	@Id
	@GeneratedValue
	long id;
	
	String userName;
	String password;
	String role;
	
	public User() {
	}
	

	public void autoAssignId() {
		id = ids.incrementAndGet();
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", userName=" + userName + ", password=" + password + ", role=" + role + "]";
	}
	
}
