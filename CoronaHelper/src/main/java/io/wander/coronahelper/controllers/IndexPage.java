package io.wander.coronahelper.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import io.wander.coronahelper.dao.WanderUserRepository;
import io.wander.coronahelper.security.User;

@Controller
public class IndexPage {
	
	@Autowired
	WanderUserRepository userRepo;

	//@GetMapping("/")
	public String indexPageRendering(Model model) {
		return "index";
	}
	
	@PostMapping("/register")
	//@ResponseBody
	public String registerUser( @RequestParam Map<String, String> body) {
		
		//TODO we need to add javascript on register page that donot submit form if password and confirm password is not same.
		// also we need to make way to propagate "Success and failure" messages to the UI.
		User user = new User();
		user.autoAssignId();
		user.setUserName(body.get("email"));
		user.setPassword(body.get("password"));
		user.setRole("user");
		
		userRepo.save(user);
		return "redirect:/login.html";
	
	}
	
}
