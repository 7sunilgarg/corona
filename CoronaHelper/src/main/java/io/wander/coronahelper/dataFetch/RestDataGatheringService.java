package io.wander.coronahelper.dataFetch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

/**
 * Service to Parse Json data from the Internet and put it into our DB by
 * converting it into desired data Model
 * 
 * @author sgarg
 *
 */
public class RestDataGatheringService {

	public final String url = "";
	
	//@JsonIgnoreProperties // Annotation used to indicate that the properties that do not match the json response should be ignored.
	// https://youtu.be/nyAoFmj6neY?t=411 -- how to create entity 

	
	// we could also use something to directly get data through rest
	@Autowired // https://youtu.be/nyAoFmj6neY?t=173
	private RestTemplate restTemplate;

	private void parse() {
		restTemplate.getForObject(url, Object.class); // Object.class can be replaced with the data model or entity of the DB
	}
}

