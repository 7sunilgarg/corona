package io.wander.coronahelper.dataFetch;

import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import io.wander.coronahelper.dao.WorldLocationStatsRepository;
import io.wander.coronahelper.data.WorldLocationStats;


@Service
public class WorldCoronaVirusDataService {
	// charting option we explored and done RnD -https://jsitor.com/CVSqNP__I
	// "https://api.covid19india.org/csv/latest/states.csv";
	private static String VIRUS_DATA_URL = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv";

	private List<WorldLocationStats> allWorldStats = new ArrayList<>();

	public List<WorldLocationStats> getAllStats() {
		return allWorldStats;
	}

	@Autowired
	WorldLocationStatsRepository worldLocationsRepo;

	@PostConstruct // Refresh after 24 Hours
	@Scheduled(fixedRate = 24 * 60 * 60 * 1000) // (fixedRate= 4 * 60 * 60 * 1000 ) // @Scheduled(cron = "* * 1 * * *")
	public void fetchVirusData() throws IOException, InterruptedException {
		List<WorldLocationStats> newStats = new ArrayList<>();
		HttpClient client = HttpClient.newHttpClient();
		HttpRequest request = HttpRequest.newBuilder().uri(URI.create(VIRUS_DATA_URL)).build();

		HttpResponse<String> httpResponse = client.send(request, HttpResponse.BodyHandlers.ofString());
		StringReader csvBodyReader = new StringReader(httpResponse.body());
		Iterable<CSVRecord> records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(csvBodyReader);

		// List<Map.Entry<Key,Value>> firstN =
		// map.entrySet().stream().limit(n).collect(Collectors.toList());

		for (CSVRecord record : records) {
			WorldLocationStats worldLocationStats = new WorldLocationStats();
			worldLocationStats.setState(record.get("Province/State"));
			worldLocationStats.setCountry(record.get("Country/Region"));
			if (worldLocationStats.getState().length() == 0) {
				worldLocationStats.setState(worldLocationStats.getCountry());
			}
			int latestCases = Integer.parseInt(record.get(record.size() - 1));
			int prevDayCases = Integer.parseInt(record.get(record.size() - 2));
			worldLocationStats.setTodayNewCases(latestCases - prevDayCases);
			worldLocationStats.setLatestTotalCases(latestCases);
			newStats.add(worldLocationStats);
		}

		this.allWorldStats = newStats;
		worldLocationsRepo.deleteAll();
		worldLocationsRepo.saveAll(newStats);
	}

	public static void main(String[] args) throws IOException, InterruptedException {

		System.out.println("Start");
		WorldCoronaVirusDataService service = new WorldCoronaVirusDataService();
		service.fetchVirusData();

		// service.getAllStats().stream().filter(x -> x).forEach(System.out::println);
		// .mapToInt( x -> x.getTodayNewCases())
		System.out.println("Stop");

	}

}
