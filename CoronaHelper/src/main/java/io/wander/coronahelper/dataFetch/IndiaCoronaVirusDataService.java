package io.wander.coronahelper.dataFetch;

import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import javax.annotation.PostConstruct;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import io.wander.coronahelper.dao.IndiaLocationStatsRepository;
import io.wander.coronahelper.data.AreaChartData;
import io.wander.coronahelper.data.FourCardsData;
import io.wander.coronahelper.data.IndiaLocationStats;

@Service
public class IndiaCoronaVirusDataService {

		// Implementation Notes :- Might be useful lateron to add more things
		// https://github.com/covid19india/api -- all data api

		// SHould not use for this Project
		// taken from web-scrapping -
		// https://api.covid19india.org/v4/min/data-2020-08-06.min.json -- just modify
		// the date in last part to get data for that day.

		// INDIA states data -- https://api.covid19india.org/csv/latest/states.csv -- we
		// already have a row with INDIA, so we dont need to process it , along with
		// TESTED column

		// private static String VIRUS_DATA_URL =
		// "https://api.covid19india.org/csv/latest/states.csv";
		private static String VIRUS_DATA_URL = "https://api.covid19india.org/csv/latest/states.csv";

		private List<IndiaLocationStats> allIndiaStats = new ArrayList<>();
		private List<IndiaLocationStats> onlyIndiaLevelStats = new ArrayList<>();
		private TreeSet<String> stateList = new TreeSet<String>();

		public List<IndiaLocationStats> getAllStats() {
			return allIndiaStats;
		}

		@Autowired
		IndiaLocationStatsRepository indiaLocationsRepo;

		@PostConstruct // Refresh after 24 Hours
		@Scheduled(fixedRate = 24 * 60 * 60 * 1000) // (fixedRate= 4 * 60 * 60 * 1000 ) // @Scheduled(cron = "* * 1 * * *")
		public void fetchVirusData() throws IOException, InterruptedException {
			List<IndiaLocationStats> newStats = new ArrayList<>();
			HttpClient client = HttpClient.newHttpClient();
			HttpRequest request = HttpRequest.newBuilder().uri(URI.create(VIRUS_DATA_URL)).build();

			HttpResponse<String> httpResponse = client.send(request, HttpResponse.BodyHandlers.ofString());
			StringReader csvBodyReader = new StringReader(httpResponse.body());
			Iterable<CSVRecord> records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(csvBodyReader);

			for (CSVRecord record : records) {
				IndiaLocationStats indiaLocationStats = new IndiaLocationStats();
				indiaLocationStats.setState(record.get("State"));
				stateList.add(indiaLocationStats.getState()); // this will give us a Unique List of Items sorted Alphabetically 
				indiaLocationStats.setDate(record.get("Date"));
				indiaLocationStats.setConfirmed(Integer.parseInt(record.get("Confirmed")));
				indiaLocationStats.setRecovered(Integer.parseInt(record.get("Recovered")));
				indiaLocationStats.setDeceased(Integer.parseInt(record.get("Deceased")));
				String tested = record.get("Tested");
				indiaLocationStats.setTested(Integer.parseInt(tested.length() > 0 ? tested : "0"));
				
				//System.out.println(indiaLocationStats);
				newStats.add(indiaLocationStats);
				// worldLocationsRepo.save(worldLocationStats);
				if (indiaLocationStats.getState().contains("India")) {
					onlyIndiaLevelStats.add(indiaLocationStats);
				}
			}
			
			this.allIndiaStats = newStats;
			indiaLocationsRepo.deleteAll();
			indiaLocationsRepo.saveAll(newStats);
			//	indiaLocationsRepo.findAllGreaterThan1000().stream().forEach(System.out::println);
			// worldLocationsRepo.findAll(null)
			populateLast15DaysData();
			
		}
		
		
		public void populateLast15DaysData() {

			int recordCount = this.onlyIndiaLevelStats.size();
			IndiaLocationStats lastIndiaLocationStatsRecord = this.onlyIndiaLevelStats.get(recordCount- 16);
			int[] newCases = new int[15];
			int[] recovered = new int[15];
			int[] deceased = new int[15];
			String[] dates = new String[15];
			IndiaLocationStats indiaLocationStats = null; 
			// we could have put a check here whether the records are greater than 15 or not, but we know that in no situation it will be coming less
			// than 15 records and if they are coming less than 15 then we need to throw exception anyways.
			for (int i = recordCount - 15, j=0; i < recordCount; i++, j++) {
				indiaLocationStats = this.onlyIndiaLevelStats.get(i); // because we want to get last 15 records.
				newCases[j] = indiaLocationStats.getConfirmed()	- lastIndiaLocationStatsRecord.getConfirmed();
				recovered[j] = indiaLocationStats.getRecovered()- lastIndiaLocationStatsRecord.getRecovered();
				deceased[j] = indiaLocationStats.getDeceased()- lastIndiaLocationStatsRecord.getDeceased();
				dates[j] = indiaLocationStats.getDate();
				lastIndiaLocationStatsRecord = indiaLocationStats;
			}
			
			if (indiaLocationStats == null || lastIndiaLocationStatsRecord == null) {
				return;
			}
			lastIndiaLocationStatsRecord = this.onlyIndiaLevelStats.get(this.onlyIndiaLevelStats.size() - 2);// we need the 2nd last records to get differ
			// now we can send this chart data through REST directly to the Chart plotting library.
			AreaChartData.chartData = new AreaChartData(newCases, recovered, deceased, dates);
			int active = indiaLocationStats.getConfirmed() - indiaLocationStats.getRecovered() -indiaLocationStats.getDeceased();
			int activeBefore = lastIndiaLocationStatsRecord.getConfirmed() - lastIndiaLocationStatsRecord.getRecovered() -lastIndiaLocationStatsRecord.getDeceased();
			int changeConfirmed = newCases[14];
			int changeRecovered = recovered[14]; 
			int changeDeceased = deceased[14]; 
			int changeActive = active - activeBefore; 
			// update information for those four cards on UI, using the last India location records
			FourCardsData.cardsData = new FourCardsData(indiaLocationStats.getConfirmed(), indiaLocationStats.getRecovered(),
											indiaLocationStats.getDeceased(), active, changeConfirmed, changeRecovered, changeDeceased, changeActive);
			
		}

		public static void main(String[] args) {

		}
}
