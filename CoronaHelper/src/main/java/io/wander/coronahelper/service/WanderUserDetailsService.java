package io.wander.coronahelper.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import io.wander.coronahelper.security.User;

@Service
public class WanderUserDetailsService //extends UserDetailsService
{
	@Autowired
	private UserRepository userRepository;
	
	// create a userRepository that will map to the user table in the database and provides CRUD operation.

//	public UserDetails loadUserByUserName(String username) {
//		User user = userRepository.findByUserName(username);
//		if (user == null) {
//			throw new Exception("User not found !!"); // we can create our own UserNotFoundException()
//		}
//		
//		
//	}
	
//	@Override // https://youtu.be/fjkelzWNSuA?t=3241
//	protected void configure(HttpSecurity http) throws Exception {
//		http
//			.csrf().disable() // why it wants to disable CSRF ?
//			.authorizeRequests().antMatchers("/login").permitAll()
//			.anyRequest().authenticated()
//			.formLogin().loginPage("/login").permitAll()
//			.and()
//			.logout().invalidateHttpSession(true)
//			.clearAuthentication(true)
//			.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
//			.logoutSuccessUrl("/logout-page or login page").permitAll();
//	}
//	
	
	
}


interface UserRepository extends JpaRepository<User, Long> {

	User findByUserName(String username);
	
}

// from telusko video https://youtu.be/fjkelzWNSuA?t=2046
// when we will have User Details from security package we will need to implement all the methods of the userDetails
//class WanderUserDetails implements UserDetails {
//	
//}

