package io.wander.coronahelper.dao;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import io.wander.coronahelper.data.IndiaLocationStats;

// Nothing to mark here
public interface IndiaLocationStatsRepository extends JpaRepository<IndiaLocationStats, Long>{
	// By default we dont need to do anything, it will give a lot of template functionality
	
	
	@Query("from IndiaLocationStats where confirmed > (select avg(confirmed) from IndiaLocationStats)")
	Collection<IndiaLocationStats> findAllGreaterThanAvg();

}
