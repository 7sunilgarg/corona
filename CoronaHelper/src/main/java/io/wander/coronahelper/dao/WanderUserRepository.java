package io.wander.coronahelper.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import io.wander.coronahelper.data.WorldLocationStats;
import io.wander.coronahelper.security.User;

// Nothing to mark here
public interface WanderUserRepository extends JpaRepository<User, Long>{

	
	// By default we dont need to do anything, it will give a lot of template functionality
	
}
