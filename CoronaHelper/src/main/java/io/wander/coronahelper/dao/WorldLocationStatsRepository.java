package io.wander.coronahelper.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import io.wander.coronahelper.data.WorldLocationStats;

// Nothing to mark here
public interface WorldLocationStatsRepository extends JpaRepository<WorldLocationStats, Long>{

	
	// By default we dont need to do anything, it will give a lot of template functionality
	
	@Query("from WorldLocationStats where todayNewCases > (select avg(todayNewCases) from WorldLocationStats)")
	 List<WorldLocationStats> findAllGreaterThanAvg();
	
}
