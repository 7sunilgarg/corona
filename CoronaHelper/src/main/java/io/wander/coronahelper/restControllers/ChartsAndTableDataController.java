package io.wander.coronahelper.restControllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import io.wander.coronahelper.dao.IndiaLocationStatsRepository;
import io.wander.coronahelper.dao.WorldLocationStatsRepository;
import io.wander.coronahelper.data.AreaChartData;
import io.wander.coronahelper.data.FourCardsData;
import io.wander.coronahelper.data.IndiaLocationStats;
import io.wander.coronahelper.data.WorldLocationStats;

@RestController
public class ChartsAndTableDataController {

	@Autowired
	WorldLocationStatsRepository worldLocationsRepo;
	
	@Autowired
	IndiaLocationStatsRepository indiaLocationRepo;
	
	@GetMapping("/indiaDailySpreadChart")
	AreaChartData indiaDailySpreadChartData() {
		return AreaChartData.chartData;
	}
	
	@GetMapping("/fourCardsData")
	FourCardsData fourCardsData() {
		return FourCardsData.cardsData;
	}
	
	@GetMapping("/indiaAtGlance")
	List<IndiaLocationStats> indiaAtGlance() {
		return indiaLocationRepo.findAllGreaterThanAvg().stream().limit(10).sorted( (x,y) -> y.getConfirmed() - x.getConfirmed()).collect(Collectors.toList());
	}
	

	@GetMapping("/worldAtGlance")
	List<WorldLocationStats> worldAtGlance() {
		return worldLocationsRepo.findAllGreaterThanAvg().stream().limit(10).sorted( (x,y) -> y.getLatestTotalCases() - x.getLatestTotalCases()).collect(Collectors.toList());
	}
}
