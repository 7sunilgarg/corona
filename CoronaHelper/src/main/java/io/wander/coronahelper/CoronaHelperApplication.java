package io.wander.coronahelper;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

@SpringBootApplication // (scanBasePackageClasses = "io.wander.*"})
@EnableAutoConfiguration
@EnableJpaRepositories("io.wander")
@EntityScan(basePackages = "io.wander")
@EnableScheduling
//@ComponentScan(basePackages = {"io.wander.coronahelper.data","io.wander.coronahelper.restControllers"})
public class CoronaHelperApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoronaHelperApplication.class, args);
		System.out.println("First Hello... ");
	}

	// Spring Boot will run ALL CommandLineRunner beans once the application context
	// is loaded.
	@Component // @Configuration anything is fine
	class DefaultLoginUserDBEntryCommandLineRunner implements CommandLineRunner {

		@Override
		public void run(String... args) throws Exception {
			// Nothing to do here ... :D
		}
	}
}
