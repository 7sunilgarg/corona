package io.wander.coronahelper.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class WorldLocationStats {

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
    private String state;
    private String country;
    private int latestTotalCases;
    private int todayNewCases;
    private int perctChange;

    public int getTodayNewCases() {
        return todayNewCases;
    }

    public void setTodayNewCases(int diffFromPrevDay) {
        this.todayNewCases = diffFromPrevDay;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getLatestTotalCases() {
        return latestTotalCases;
    }

    public void setLatestTotalCases(int latestTotalCases) {
        this.latestTotalCases = latestTotalCases;
    }

	@Override
	public String toString() {
		return "WorldLocationStats [id=" + id + ", state=" + state + ", country=" + country + ", latestTotalCases="
				+ latestTotalCases + ", todayNewCases=" + todayNewCases + "]";
	}

	public int getPerctChange() {
		return perctChange;
	}

	public void setPerctChange(int perctChange) {
		this.perctChange = perctChange;
	}

   
}
