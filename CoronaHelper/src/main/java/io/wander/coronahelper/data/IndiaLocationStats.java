package io.wander.coronahelper.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class IndiaLocationStats {
	
	// we will plot graph for only India for now on top of the page for confirmed, recovered, deceased, and tested.

	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY) // how to get different sequence generator need to check
	private long id;
	
	private String date;
	private String state;
	private int confirmed;
	private int recovered;
	private int deceased;
	private int tested;
	
	public IndiaLocationStats() { // why JPA why ?
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public int getConfirmed() {
		return confirmed;
	}
	public void setConfirmed(int confirmed) {
		this.confirmed = confirmed;
	}
	public int getRecovered() {
		return recovered;
	}
	public void setRecovered(int recovered) {
		this.recovered = recovered;
	}
	public int getDeceased() {
		return deceased;
	}
	public void setDeceased(int deceased) {
		this.deceased = deceased;
	}
	public int getTested() {
		return tested;
	}
	public void setTested(int tested) {
		this.tested = tested;
	}

	@Override
	public String toString() {
		return "IndiaLocationStats [id=" + id + ", date=" + date + ", state=" + state + ", confirmed=" + confirmed
				+ ", recovered=" + recovered + ", deceased=" + deceased + ", tested=" + tested + "]";
	}
	
	
	
}
