package io.wander.coronahelper.data;

import java.util.Arrays;

/**
 * Chart Data POJO for Indian Chart View
 * 
 * 
 * @author sgarg
 */
public class AreaChartData {
	
	// this chartdata will be refreshed after every 24 hours by scheduler with new data, which is being fetched from the data source
	public static AreaChartData chartData = new AreaChartData();
	
	int[] newCases;
	int[] recovered;
	int[] deceased;
	String[] dates;
	
	public AreaChartData() {
	}
	
	public AreaChartData(int[] newCases, int[] recovered, int[] deceased, String[] dates) {
		this.newCases = newCases;
		this.recovered = recovered;
		this.deceased = deceased;
		this.dates = dates;
	}

	public int[] getConfirmed() {
		return newCases;
	}

	public void setConfirmed(int[] confirmed) {
		this.newCases = confirmed;
	}

	public int[] getRecovered() {
		return recovered;
	}

	public void setRecovered(int[] recovered) {
		this.recovered = recovered;
	}

	public int[] getDeceased() {
		return deceased;
	}

	public void setDeceased(int[] deceased) {
		this.deceased = deceased;
	}

	public int[] getNewCases() {
		return newCases;
	}

	public void setNewCases(int[] newCases) {
		this.newCases = newCases;
	}

	public String[] getDates() {
		return dates;
	}

	public void setDates(String[] dates) {
		this.dates = dates;
	}

	@Override
	public String toString() {
		return "ChartData [newCases=" + Arrays.toString(newCases) + ", recovered=" + Arrays.toString(recovered)
				+ ", deceased=" + Arrays.toString(deceased) + ", dates=" + Arrays.toString(dates) + "]";
	}
	
	
	
}
