package io.wander.coronahelper.data;

/**
 * this class will represent the latest data of the cards that will be sent to display latest confirmed, 
 * recovered, deceased and active.
 * 
 * @author sgarg
 *
 */
public class FourCardsData {
	
	public static FourCardsData cardsData = new FourCardsData();
	
	int confirmed;
	int recovered;
	int deceased;
	int active;
	
	int changeConfirmed;
	int changeRecovered;
	int changeDeceased;
	int changeActive;
	
	
	public FourCardsData() {
	}


	public FourCardsData(int confirmed, int recovered, int deceased, int active, int changeConfirmed,
			int changeRecovered, int changeDeceased, int changeActive) {
		super();
		this.confirmed = confirmed;
		this.recovered = recovered;
		this.deceased = deceased;
		this.active = active;
		this.changeConfirmed = changeConfirmed;
		this.changeRecovered = changeRecovered;
		this.changeDeceased = changeDeceased;
		this.changeActive = changeActive;
	}


	public static FourCardsData getCardsData() {
		return cardsData;
	}


	public static void setCardsData(FourCardsData chartData) {
		FourCardsData.cardsData = chartData;
	}


	public int getConfirmed() {
		return confirmed;
	}


	public void setConfirmed(int confirmed) {
		this.confirmed = confirmed;
	}


	public int getRecovered() {
		return recovered;
	}


	public void setRecovered(int recovered) {
		this.recovered = recovered;
	}


	public int getDeceased() {
		return deceased;
	}


	public void setDeceased(int deceased) {
		this.deceased = deceased;
	}


	public int getActive() {
		return active;
	}


	public void setActive(int active) {
		this.active = active;
	}


	public int getChangeConfirmed() {
		return changeConfirmed;
	}


	public void setChangeConfirmed(int changeConfirmed) {
		this.changeConfirmed = changeConfirmed;
	}


	public int getChangeRecovered() {
		return changeRecovered;
	}


	public void setChangeRecovered(int changeRecovered) {
		this.changeRecovered = changeRecovered;
	}


	public int getChangeDeceased() {
		return changeDeceased;
	}


	public void setChangeDeceased(int changeDeceased) {
		this.changeDeceased = changeDeceased;
	}


	public int getChangeActive() {
		return changeActive;
	}


	public void setChangeActive(int changeActive) {
		this.changeActive = changeActive;
	}


	@Override
	public String toString() {
		return "FourCardsData [confirmed=" + confirmed + ", recovered=" + recovered + ", deceased=" + deceased
				+ ", active=" + active + ", changeConfirmed=" + changeConfirmed + ", changeRecovered=" + changeRecovered
				+ ", changeDeceased=" + changeDeceased + ", changeActive=" + changeActive + "]";
	}
	
	
}
