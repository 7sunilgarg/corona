var options = {
  plotOptions: {
    treemap: {
  enableShades: true,
        shadeIntensity: 0.5,
        reverseNegativeShade: true,
        distributed: false,
        useFillColorAsStroke: false,
      colorScale: {
        ranges: [
          {
            from: 250,
            to: 350,
            color: '#a20000'
          }, {
            from: 100,
            to: 250,
            color: '#CD363A'
          },
          {
            from: 0,
            to: 50,
            color: '#52B12C'
          }
        ]
      }
    }
  },
  series: [
    {
      data: [
        {
          x: 'Mumbai',
          y: 284
        },
        {
          x: 'New Delhi',
          y: 118
        },
        {
          x: 'Kerala',
          y: 149
        },
        
        {
          x: 'Ahmedabad',
          y: 55
        },
        {
          x: 'Bangaluru',
          y: 84
        },
        {
          x: 'Pune',
          y: 31
        },
        {
          x: 'Chennai',
          y: 70
        },
        {
          x: 'Jaipur',
          y: 30
        },
        {
          x: 'Surat',
          y: 44
        },
        {
          x: 'Hyderabad',
          y: 68
        },
        {
          x: 'Lucknow',
          y: 28
        },
        {
          x: 'Indore',
          y: 19
        },
        {
          x: 'Kanpur',
          y: 29
        }
      ]
    }
  ],
  legend: {
    show: false
  },
  chart: {
    height: 350,
    type: 'treemap'
  }
};

var chart = new ApexCharts(document.querySelector("#IndiaHeapMap"), options);
chart.render();
