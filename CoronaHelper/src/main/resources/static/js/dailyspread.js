$(document).ready(function(){
var seriesData = [{name: 'New Cases(*100)', data : []},{name : 'Recovered(*100)', data : []}, {name : 'deceased', data : []}, {name : 'Confirmed(*100)', data : []}];
var categoryData = [];		
		// Ajax call
		var url  = "/indiaDailySpreadChart";
		$.getJSON(url, function( data ) {
			//debugger;
		    seriesData[0]['data'] = data.newCases.map(x => x/100);
		    seriesData[1]['data'] = data.recovered.map(x => x/100);
		    seriesData[2]['data'] = data.deceased;
		    seriesData[3]['data'] = data.confirmed.map(x => x/100);
		    categoryData = data.dates;
	
		var options = {
		  series: seriesData,
		  chart: {
		    height: 350,
		    type: 'area'
		  },
		  dataLabels: {
		    enabled: true
		  },
		  stroke: {
		    curve: 'smooth'
		  },
		  xaxis: {
		    type: 'datetime',
		    categories: categoryData
		  },
		  tooltip: {
		    x: {
		      format: 'dd/MM/yy'
		    },
		  },
		};
		
		
		var chart = new ApexCharts(document.querySelector("#IndiaDailySpreadTrend"), options);
		chart.render();
	});
}); 

