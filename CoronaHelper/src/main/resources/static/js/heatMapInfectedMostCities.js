// Replace Math.random() with a pseudo-random number generator to get reproducible results in e2e tests
// Based on https://gist.github.com/blixt/f17b47c62508be59987b
var _seed = 42;
Math.random = function () {
  _seed = _seed * 16807 % 2147483647;
  return (_seed - 1) / 2147483646;
};

function generateData(count, yrange) {
  var i = 0;
  var series = [];
  while (i < count) {
    var x = (i + 1).toString();
    var y = Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min;

    series.push({
      x: x,
      y: y
    });
    i++;
  }
  return series;
}

var data = [
  {
    name: 'Delhi',
    data: generateData(8, {
      min: 0,
      max: 90
    })
  },
  {
    name: 'Mumbai',
    data: generateData(8, {
      min: 0,
      max: 90
    })
  },
  {
    name: 'Kerela',
    data: generateData(8, {
      min: 0,
      max: 90
    })
  },
  {
    name: 'Bengaluru',
    data: generateData(8, {
      min: 0,
      max: 90
    })
  },
  {
    name: 'Kolakata',
    data: generateData(8, {
      min: 0,
      max: 90
    })
  },
  {
    name: 'Ahmedabad',
    data: generateData(8, {
      min: 0,
      max: 90
    })
  },
  {
    name: 'Pune',
    data: generateData(8, {
      min: 0,
      max: 90
    })
  },
  {
    name: 'Chennai',
    data: generateData(8, {
      min: 0,
      max: 90
    })
  },
  {
    name: 'Hyderabad',
    data: generateData(8, {
      min: 0,
      max: 90
    })
  },
  {
    name: 'Jaipur',
    data: generateData(8, {
      min: 0,
      max: 90
    })
  },
  {
    name: 'Surat',
    data: generateData(8, {
      min: 0,
      max: 90
    })
  },
  {
    name: 'Lucknow',
    data: generateData(8, {
      min: 0,
      max: 90
    })
  },
  {
    name: 'Kanpur',
    data: generateData(8, {
      min: 0,
      max: 90
    })
  },
  {
    name: 'Indore',
    data: generateData(8, {
      min: 0,
      max: 90
    })
  },
  {
    name: 'Thane',
    data: generateData(8, {
      min: 0,
      max: 90
    })
  }
]

data.reverse()

var colors = ["#F3B415", "#F27036", "#663F59", "#6A6E94", "#4E88B4", "#00A7C6", "#18D8D8", '#A9D794', '#46AF78', '#A93F55', '#8C5E58', '#2176FF', '#33A1FD', '#7A918D', '#BAFF29']

colors.reverse();

var options = {
  series: data,
  chart: {
    height: 450,
    type: 'heatmap',
  },
  dataLabels: {
    enabled: false
  },
  colors: colors,
  xaxis: {
    type: 'category',
    categories: ['Jan,21', 'Feb,21', 'Mar,21', 'Apr,21', 'May,20', 'Jun,20', 'Jul,20', 'Aug,20', 'Sep,20', 'Oct,20', 'Nov,20', 'Dec,20']
  },
  title: {
    text: 'Indexed from 0-100  [0-Best, 100-worst]'
  },
  grid: {
    padding: {
      right: 20
    }
  }
};

var chart = new ApexCharts(document.querySelector("#chart"), options);
chart.render();

