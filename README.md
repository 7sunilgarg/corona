# Corona Helper or Dashboard Analytics

The Graphs and Tables :-
The graph are dynamic in nature in a sense that when you hove on them, they will show you data in tooltip.
You can zoom in and zoom out a particular area or timeline, this way the area will be expanded and it can analysed in a better way. 

The data for these graphs are not fixed in the backend and is being fetched from two different sources one for India data and one for world data.
Moreover the data we get is in different forms i.e. not in the form where we can consume it directly, so data was transformed and stored in different form.

As we expect to montior the production system for different services  running and their health, I have added a metrics control system, though for time-being its json based but shows the real process and API monitoring.
http://"IP":8080/actuator/health   --- Application health status
http://"IP":8080/actuator   --- All endpoints


What I have learnt with this execise or Project Creation
1. Domain data modeling, More UX/UI
2. Spring, Spring boot, spring MVC, Spring Security, OAuth2, Hibernate, writing better REST services using HATEOS model, 
3. Learnt fetching data from REST and also from webservices 


About UI

Register page does not have any client side check to see whether password and confirm password match or not, the server will simply use the username/email and password to store user. lateron we might add success and failure message, but for now it has nothing client side checks.

Similarly the login page does not check for any authenticity of the user and simply redirects you to the index.html or dashboard, the Application security layer is not implemented as It will take quite some time to understand spring internal filter structure and how to mold it with my custom pages and resource so I have skipped for this implementation as due to little tight schedule.

UI is based on a bootstrap theme that is responsive and can adapt itself to the screen size i.e. it will behave properly and show content as per the size of the device screen, whether on mobile or Tablet or on wide screen.

Some parts of the UI are for future purpose and are not implemented like Search bar, which in future can show graphs and other table as per the searched City or country.
All Pages are built using bootstrap, a free to use library.

The profile or setting page is dummy and does not fetch anything from database.
The forgot password link and remember me link on login page is not yet implemented as they are for now out of scope.

On Dashboard :
The Notification Icon and the message icon is not implemented and is with dummy data.
The drop down menu on user photo click has different menu item which are not implemented except for logout button
